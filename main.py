import numpy as np
import keras as K
import tensorflow as tf
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def main():

    #Reading and loading dataset

    print("\nIris dataset using Keras/TensorFlow")
    np.random.seed(4)
    tf.random.set_seed(13)
    
    print("Loading Iris data into memory \n")
    
    with open('iris_dataset_train.csv', 'r') as f_in, open ('iris_dataset_train.txt', 'w') as f_out:
        content = f_in.read()
        f_out.write(content)
    
    with open('iris_dataset_test.csv', 'r') as f_in, open ('iris_dataset_test.txt', 'w') as f_out:
        content  = f_in.read()
        f_out.write(content)
 
    train_file = 'iris_dataset_train.txt'
    test_file = 'iris_dataset_test.txt'

    train_x = np.loadtxt(train_file, usecols=[0,1,2,3], delimiter=",", skiprows=0, dtype=np.float32)
    train_y = np.loadtxt(train_file, usecols=[4,5,6], delimiter=",", skiprows=0, dtype=np.float32)

    test_x = np.loadtxt(test_file, usecols=range(0,4), delimiter=",", skiprows=0, dtype=np.float32)
    test_y = np.loadtxt(test_file, usecols=range(4,7), delimiter=",", skiprows=0, dtype=np.float32)

    print("Done loading data from dataset")

    #Defining Neural Network model -> 4-(5-6)-3 deep neural network

        # 4 input values (one for each flower variable)
        # 2 hidden layers with 5 and 6 nodes respectively
        # 3 outputs nodes (one for each possible species of flower)

    init = K.initializers.glorot_uniform(seed=1) #this initialization function set random small values of weight and biases
    simple_adam = K.optimizers.Adam()
    model = K.models.Sequential()
    model.add(K.layers.Dense(units=5, input_dim=4, kernel_initializer=init, activation='relu')) #input layer is implicit, model begins with hiden layer
    model.add(K.layers.Dense(units=6, kernel_initializer=init, activation='relu'))
    model.add(K.layers.Dense(units=3, kernel_initializer=init, activation='softmax'))
    
    print('Compiling model')
    
    model.compile(loss='categorical_crossentropy', optimizer=simple_adam, metrics=['accuracy']) #cross-entropy measures the distance from softmax output predicts and truth values. The objective is to minimize cross-entropy 

    print('Done compiling model')

    #Train model

    b_size = 1 #NN weight and biases are updated for every training item. Batch size couls be numb of itens in training set (120)(full-batch training) or an intermediate value (16)(mini-batch training)
    max_epochs = 10 #how many iterations will be used for training
    print("Start training model")
    h = model.fit(train_x, train_y, batch_size=b_size, epochs=max_epochs, shuffle=True, verbose=1) # fit() returns traning history, were can see loss values. Is stored in variable h
    print("Done training model")
    print("----------Training History----------")
    print(h.history['loss'])

    #Evaluate model on test data
    
    eval = model.evaluate(test_x, test_y, verbose=0) #returns list of values: [0] value of loss function. Other values are any opitional metrics from compile() function. In this case [1] is accuracy
    print("Evaluation on test data: loss = %0.6f accuracy = %0.2f%% \n" % (eval[0], eval[1]*100))  

    #Save model
    print("Saving model to disk")
    mp = ".\\Models\\iris_model.h5"
    model.save(mp)
    print("Model Saved")

    #Using the model
    np.set_printoptions(precision=4)
    unknown = np.array([[6.1, 3.1, 5.1, 1.1]], dtype=np.float32)
    predicted = model.predict(unknown) #prediction is based on current model weights and biases
    print("Using model to predict species for features: ")
    print(unknown)
    print("\nPredicted species is: ")
    print(predicted)
    labels = ["setosa", "versicolor", "virginica"]
    idx = np.argmax(predicted)
    species = labels[idx]
    print(species)
main()
